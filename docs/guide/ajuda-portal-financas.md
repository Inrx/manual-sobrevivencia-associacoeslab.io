# Ajuda no portal finanças

O [portal das
finanças](https://www.portaldasfinancas.gov.pt/at/html/index.html) é
particularmente conhecido por dar muitas dores de cabeça. Este
apêndice do manual pretende suavizar as várias operações.

## Alterar membros da direção

1. Ir ao https://www.portaldasfinancas.gov.pt
2. Navegar: “todos os serviços” » “alteração de atividade” » “entrega
    de alteração de atividade” » “entregar declaração” » “entrega de
    declaração de alteração de atividade”
3. Remover “secretário” com data de término de funções
4. Adicionar novo secretário


