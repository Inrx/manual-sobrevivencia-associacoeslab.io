# Modelo de relatório e contas para uma associação

## Objetivos

O relatório e contas é um documento que tem como objetivos:

* Apresentar as demonstrações financeiras da associação;
* Prestar contas aos associados, parceiros e colaboradores;
* Assegurar a transparência e a gestão responsável da associação.

## Estrutura

O relatório e contas deve ser dividido nas seguintes partes:

* Relatório: apresenta uma análise da atividade da associação durante o período em análise;
* Demonstrações financeiras: apresentam as receitas e despesas da associação, bem como o seu património;
* Parecer do Conselho Fiscal: emite uma opinião sobre a exatidão e a transparência das demonstrações financeiras.

## Relatório

A parte do relatório dedicada à análise da atividade da associação deve apresentar um resumo das ações realizadas, com destaque para as mais relevantes. Para isso, devem ser considerados os seguintes critérios:

* Alcance dos objetivos;
* Qualidade das atividades desenvolvidas;
* Impacto das atividades na sociedade.

## Demonstrações financeiras

As demonstrações financeiras devem ser elaboradas de acordo com os princípios contabilísticos geralmente aceites. As principais demonstrações financeiras são:

* Balanço: apresenta o património da associação, incluindo os ativos, os passivos e o capital próprio;
* Demonstração de resultados: apresenta as receitas e despesas da associação, bem como o seu resultado líquido;
* Demonstração de fluxos de caixa: apresenta os fluxos de caixa da associação, incluindo os fluxos de caixa operacionais, os fluxos de caixa de investimento e os fluxos de caixa financeiros.

## Parecer do Conselho Fiscal

O parecer do Conselho Fiscal deve emitir uma opinião sobre a exatidão e a transparência das demonstrações financeiras.

## Exemplo

### Associação de Pais e Encarregados de Educação da Escola Secundária Gabriel Pereira

**Relatório e Contas 2022-2023**

**Relatório**

A Associação de Pais e Encarregados de Educação da Escola Secundária Gabriel Pereira (APESG) é uma associação sem fins lucrativos que tem como objetivo promover a participação dos pais e encarregados de educação na vida da escola.

Este relatório e contas apresenta as demonstrações financeiras da APESG durante o ano letivo de 2022-2023.

**Demonstrações financeiras**

**Balanço**

Ativo

    Caixa e equivalentes de caixa: €10.000
    Contas a receber: €20.000
    Equipamentos: €30.000

Passivo

    Contas a pagar: €10.000
    Empréstimos: €20.000
    Capital próprio: €60.000

**Demonstração de resultados**

Receitas

    Subsídios: €50.000
    Donativos: €20.000
    Receitas operacionais: €30.000

Despesas

    Despesas operacionais: €50.000
    Despesas financeiras: €20.000

Resultado líquido: €20.000

**Demonstração de fluxos de caixa**

Fluxos de caixa operacionais

    Caixa das atividades operacionais: €20.000

Fluxos de caixa de investimento

    Caixa das atividades de investimento: €10.000

Fluxos de caixa financeiros

    Caixa das atividades de financiamento: €10.000

Variação da tesouraria: €20.000

**Parecer do Conselho Fiscal**

O Conselho Fiscal emitiu uma opinião favorável sobre as demonstrações financeiras da APESG. O Conselho Fiscal considera que as demonstrações financeiras são apresentadas de forma clara e transparente, e que refletem com exatidão a situação financeira da associação.

Este modelo pode ser adaptado às necessidades específicas de cada associação. No entanto, é importante que o relatório e contas seja elaborado com cuidado, pois é um documento que garante a transparência e a gestão responsável da associação.

Aqui estão algumas dicas para elaborar um relatório e contas eficiente:

* Utilize um modelo de relatório e contas adequado às necessidades da associação: existem vários modelos de relatório e contas disponíveis online ou em softwares de contabilidade.
* Colete todos os dados necessários: as demonstrações financeiras devem ser elaboradas com base em dados fiáveis e atualizados.
* Revise o relatório e contas cuidadosamente: antes de submeter o relatório e contas, certifique-se de que está completo e sem erros.
