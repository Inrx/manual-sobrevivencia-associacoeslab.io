# Após criar a associação

::: warning AVISO

Este manual ainda está em desenvolvimento e como tal, não recomendamos
ainda o seu uso. Caso deseje ajudar, veja como
[contribuir](contribuir.md).

:::

[[toc]]

O primeiro passo já está feito. A associação é finalmente uma entidade
legal e já com membros nos [órgãos sociais](glossario.md#orgaos-sociais).

No entanto, como sabemos, em Portugal as coisa não acontecem
automágicamente. Às vezes, até [por boas
razões](https://web.archive.org/web/20200130124139/http://www.apdsi.pt/wp-content/uploads/prev/relato.pdf)
mas isso significa que, ao criarmos oficialmente a associação, a
segurança social e a autoridade tributária não têm necessariamente
conhecimento disso. Então é necessário fazer mais alguns passos até
ser completamente uma associação.

Criar carimbo
-------------

Quando se assina em nome da associação, deve ser carimbado com o carimbo oficial da associação. Portanto um deve ser criado. Pode ser encomendado em lojas como a Staples.

Elemetos a incluír:
  - nome legal da associação
  - morada
  - NIPC

::: tip DICA

Atualmente a criação de um carimbo não é completamente necessária. Por exemplo, para a segurança social poderão informar que não possuem carimbo. Mas mais cedo ou mais tarde pode ser necessário. E para esses casos, é melhor arranjar.

:::


Criar conta bancária
--------------------

Gerir o dinheiro por baixo da almofada não é viável (e se calhar nem
sequer legal!) para associações. Portanto, é preciso criar uma conta
bancária.

| Banco            | Recomendação    | Vantagens                | Desvantagens                                                    |
|------------------|-----------------|--------------------------|-----------------------------------------------------------------|
| Montepio         | Não recomendado     | É uma associação mutualista e pode estar mais recetiva a associações | 500€ p/ abrir conta; Obrigam **todos** os orgãos sociais a criarem conta no banco. E sempre que há eleições as 9 ou mais pessoas têm que ir ao Montepio atualizar os dados        |
| Millenium BCP    | **Recomendado** | Não tem custos para ONGs | Todas as operações são demoradas e implica deslocações ao banco |
| Crédito Agrícola | NÃO recomendado     | apenas 5,50€/mês manutenção                        | Recusa-se a abir contas para associações (2021-03-18); 500€ min para abrir conta; Não está disponível em todo o país                              |
| BPI              | NÃO recomendado | -                        | 7.5€/mês de custo; práticas deploráveis de dificultar burocráticamente o processo de criação de conta a associações |
| Caixa            | **Recomendado** | Quando os documentos de criação da associação estão todos em ordem a conta pode ficar aberta em poucos dias        | 500€ p/ abrir conta; Muito picuinhas quanto ao registos em atas |
| Banco CTT        | por avaliar     |                          |                                                                 |
| Santander        | por avaliar     |                          |                                                                 |
| Novo Banco       | por avaliar     |                          |                                                                 |


::: tip DICA

Assumam que vão gastar muito tempo a abrir a conta. O banco vai ter
que fazer montes de verificações, pedir imensas informações pessoais,
às vezes de forma repetida.

:::


Finanças: Declaração de início de atividade
-------------------------------------------

#### Como entregar Declaração do Início da Atividade?
Por último, a associação deve entregar a declaração do início da atividade, presencialmente, na [Repartição de Finanças](http://info.portaldasfinancas.gov.pt/pt/dgci/contactos_servicos/enderecos_contactos/) da área onde fica a sede social da entidade, ou através da Internet, no [Portal das Finanças](http://www.portaldasfinancas.gov.pt/pt/home.action), e assim regularizar a sua situação relativamente ao cumprimento das obrigações fiscais. ([source](https://acontece.torresnovas.pt/faq))

- **online** - pode ser feito online, mas só por um contabilista certificado (TOC - Técnico Oficial de Contas) ([FAQ pergunta 21][FAQ Associação na Hora]). Para tal basta darmos as indicações de qual o/a contabilista responsável.

- **online** - desde a pandemia por COVID-19, é possível para uma associação sem contabilidade organizada - logo, sem TOC - abrir atividade no Portal das Finanças através da plataforma [E-Balcão](https://sitfiscal.portaldasfinancas.gov.pt/ebalcao/formularioContacto).

- **offline** - dirigindo-nos presencialmente à repartição de finanças ou pedindo logo no momento de criação da associação (num passo anterior)

> Podemos não entregar estes dados para o início de atividade, mas
> temos um prazo de 90 dias para o fazer -- [lei 40/2007 artigo
> 6º ponto 3](https://dre.pt/pesquisa/-/search/640908/details/maximized)

::: tip DICA

Caso tenha criado a associação pelo [processo
tradicional](criar-associacao.md#via-processo-tradicional) este passo
poderá já ter sido tratado no ato de constituição. Confirmar isso.

:::

Após a declaração devemos receber um cartão de associação em suporte
físico (enviado para a morada da associação ou para a morada de quem o
pede)

::: warning AVISO

:gun: **fuzilamento legal**: O estado não perdoa. Têm três meses para declarar o início de
atividade da associação mesmo que ainda não tenham mexido num único tostão. A multa é de 150 €.

:::


Registar Beneficiário Efetivo (online)
--------------------------------------

Este registo é uma medida anti-branqueamento de capitais implementada
a nível europeu. Mas na verdade é uma medida bastante dracónica quando
estamos a falar de pequenas associações ainda nem lidam com mais que
1000 € e ainda mais irónico é o facto de ser a direção (voluntária)
ainda tem que se sujeitar a levar com as culpas, quando na verdade os
benecificiários são todos os sócios.

::: warning Aviso

:gun: **fuzilamento legal**: A submissão tardia deste registo implica uma multa entre 1000€ e
50.000€ essencialmente endividando a pequena associação até à morte.

:::

Essencialmente é necessário submeter no portal [RCBE](https://rcbe.justica.gov.pt/) as informações
pessoais dos membros da direção (beneficiários efetivos).

Requisitos:

| Requisito                   | Descrição                               |
|-----------------------------|-----------------------------------------|
| Dados dos membros           | nome, morada, NIF, país de origem, etc. |
| Método de autenticação      | ver informação abaixo                   |

::: details Métodos de autenticação

Métodos de autenticação possíveis:
  
  - **CC + Leitor de CC** - um leitor de smart-cards que permita ler o CC e [applicação Autenticação.gov](https://www.autenticacao.gov.pt/web/guest/aplicacao/aplicacao-autenticacao-gov)
  
  - **chave móvel digital** - mais informação [aqui](https://www.autenticacao.gov.pt/cmd-pedido-chave)
  
  - **assinatura digital do CC** - mais informação [aqui](https://www.autenticacao.gov.pt/web/guest/assinatura-digital/assinatura-digital-qualificada)
  
  - **advogado ou solicitador** - caso a associação tenha advogado ou solicitador, estes poderão usar o seu certificado.

Tanto a chave móvel como a assinatura digital do CC, o que pode ser ativada numa
loja do cidadão ou em qq junta da freguesia (e não esquecer de levar o telemóvel
/ ter telemóvel operacional (com carga), pois sem ele nada feito!).

:::

::: warning Aviso

Após submetido os dados, o sistema enviará a todos os membros da
direção um email com todos os dados submetidos. Isto significa que
entre outras coisas, os membros passarão a saber a casa uns dos
outros, que pode não ser desejável.

:::


Registo na Segurança Social (SS)
--------------------------------

Caso os órgãos sociais não sejam remunerados, a coisa é mais
fácil. Mas a SS precisa de saber quem são essas pessoas e saber que
não têm vencimento na associação para não terem de fazer descontos.

É possivel que a associação receba uma carta da Segurança Social a
pedir que se declare que os membros estatutários (MOEs) não recebem
vencimentos da associação. Nesse caso, deve bastar responderem à
carta.

Caso não a recebam essa carta (TODO dizer prazo). É necessário ir à SS
[os membros da direção](glossario.md#A-associacao-obriga-se-a-X) para
fazer este registo (TODO: verificar se não dá para fazer online).

::: tip DICA

Para evitar problemas, o melhor é incluirem na ata de eleição dos
[órgãos sociais](glossario.md#orgaos-sociais) que estes não são
remunerados.

:::

documentos necessários:

| Documento                        | Descrição                                                                        |
|----------------------------------|----------------------------------------------------------------------------------|
| Cartão com NIF PC                | Cartão de identificação da associação que tem o seu NIF                                                                |
| Cartão Seg. Social dos MOEs      | Cartão de Seg. Social dos [MOEs](glossario.md#membros-de-orgaos-estatutarios) OU doc. comprovativodo enquadramento noutro sistema de proteção social OU Cópia de Cartão de Cidadão |
| Escritura de Constituição        | Escritura de constituição ou registo na consevatório do registo comercial ou cópia publicação em Diário da República |
| Livro de atas                    | como prova de quem são os [MOEs](glossario.md#membros-de-orgaos-estatutarios). Nesta ata tem que estar escrito que não são remunderados. |    |
| Doc. início de atividade         | Documento fiscal que comprove o início de atividade                              |
| Cartao Segurança Social dos MOEs | Ou documento comprovativo do seu enquadramento noutro sistema de proteção social |
| Cópia de Identificações dos MOEs | Cartão do cidadão ou caso seja de outro país, uma cópia da(o) sua(seu) ID        |
| Modelo RV 1011                   | Comunicação de início de atividade. [Formulário no site da SS](https://seg-social.pt/documents/10152/39019/RV_1011_DGSS/a1354efd-e4af-40b5-b4ef-c6b8f2b62226).                                                      |
| Declarações de IRC*              | *Apenas necessário caso já se tenha fetio alguma submissão para IRC               |

::: details Nota para cidadãos estrangeiros como MOEs

Nota: caso de algum dos MOEs não ser português e não tiver já um
[NISS](http://www.seg-social.pt/pedido-de-niss1) é necessário obtê-lo
com dois documentos adicionais: o `1009 DGSS` e o `1006
DGSS`. Perguntar sobre isto na segurança social.

:::
